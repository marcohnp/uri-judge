package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1020 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int anos, meses, dias;
		int restoAnos, restoMeses;
		
		int idadeEmDias = sc.nextInt();
		
		anos = idadeEmDias / 365;
		restoAnos = idadeEmDias % 365;
		meses = restoAnos / 30;
		restoMeses = restoAnos % 30;
		dias = restoMeses;
		
		System.out.printf("%d ano(s)\n", anos);
		System.out.printf("%d mes(es)\n", meses);
		System.out.printf("%d dia(s)\n", dias);
		
		
		sc.close();

	}

}
