package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1010 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int codPieceA, codPieceB, quantityA, quantityB;
		double priceA, priceB, total;
		
		codPieceA = sc.nextInt();
		quantityA = sc.nextInt();
		priceA = sc.nextDouble();
		
		codPieceB = sc.nextInt();
		quantityB = sc.nextInt();
		priceB  = sc.nextDouble();
		
		total = (quantityA * priceA) + (quantityB * priceB);
		
		System.out.printf("VALOR A PAGAR: R$ %.2f\n", total);
		
		sc.close();
	
	}

}
