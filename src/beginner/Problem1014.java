package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1014 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int km;
		double consumption, averageConsumption;
		
		km = sc.nextInt();
		consumption = sc.nextDouble();
		
		averageConsumption = km / consumption;
		
		System.out.printf("%.3f km/l\n", averageConsumption);
		
		
		sc.close();

	}

}
