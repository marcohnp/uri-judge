package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1017 {

		public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int tempo, velocidadeMedia;
		double litros;
		
		tempo = sc.nextInt();
		velocidadeMedia = sc.nextInt();
		
		litros = (tempo * velocidadeMedia) / 12.0;
		
		System.out.printf("%.3f\n", litros);
		
		sc.close();
			
	}
}
