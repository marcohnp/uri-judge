package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1008 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int number, hours;
		double salaryPerHour, salary;
		
		number = sc.nextInt();
		hours = sc.nextInt();
		salaryPerHour = sc.nextDouble();
		
		salary = hours * salaryPerHour;
		
		System.out.printf("NUMBER = %d\n", number);
		System.out.printf("SALARY = U$ %.2f\n", salary);
		
		
		sc.close();
	}

}
