package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1016 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int km, tempo;
		
		km = sc.nextInt();
		
		tempo = km * 2;
		
		System.out.printf("%d minutos\n", tempo);
		
		sc.close();
	}

}
