package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1012 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double a, b, c, pi = 3.14159;
		double triangle, circle, trapeze, square, rectangle;
		
		a = sc.nextDouble();
		b = sc.nextDouble();
		c = sc.nextDouble();
		
		triangle = (a * c) / 2.0;
		circle = pi * Math.pow(c, 2.0);
		trapeze = ((a + b) * c) / 2.0;
		square = b * b;
		rectangle = a * b;
		
		System.out.printf("TRIANGULO: %.3f\n", triangle);
		System.out.printf("CIRCULO: %.3f\n", circle);
		System.out.printf("TRAPEZIO: %.3f\n", trapeze);
		System.out.printf("QUADRADO: %.3f\n", square);
		System.out.printf("RETANGULO: %.3f\n", rectangle);
		
		sc.close();

	}

}
