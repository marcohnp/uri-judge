package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1019 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int horas, minutos, segundos, tempoEmSegundos;
		int restoHoras, restoMinutos;
		
		tempoEmSegundos = sc.nextInt();
		
		horas = tempoEmSegundos / (60 * 60);
		restoHoras = tempoEmSegundos % (60 * 60);
		minutos = restoHoras / 60;
		restoMinutos = restoHoras % 60;
		segundos = restoMinutos;
		
		System.out.printf("%d:%d:%d\n", horas, minutos, segundos);
		
		
		sc.close();

	}

}
