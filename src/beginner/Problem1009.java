package beginner;

import java.util.Locale;
import java.util.Scanner;

public class Problem1009 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double salary, sales;
		String name;
		
		name = sc.nextLine();
		salary = sc.nextDouble();
		sales = sc.nextDouble();
		
		salary = salary + (sales * 0.15);
		
		System.out.printf("TOTAL = R$ %.2f\n", salary);
		
		sc.close();

	}

}
